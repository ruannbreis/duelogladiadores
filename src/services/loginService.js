const api = 'http://localhost:8000/api/v1'
export default {

    login (ctx, params, success, failed) {

        delete ctx.axios.defaults.headers.post["Content-Type"];
        delete ctx.axios.defaults.headers.post["Accept"];
        delete ctx.axios.defaults.headers.common["Content-Type"];
        delete ctx.axios.defaults.headers.common["Accept"];

        delete ctx.axios.defaults.headers.put["Content-Type"];
        delete ctx.axios.defaults.headers.put["Accept"];

        delete ctx.axios.defaults.headers.patch["Content-Type"];
        delete ctx.axios.defaults.headers.patch["Accept"];
        ctx.axios({
            method: 'post',
            url: api + '/login' ,
            params: params
        }).then(success)
        .catch(failed);
    },
}