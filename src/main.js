import Vue from 'vue'
import App from './App.vue'
import Duelo from './components/Duelo.vue'
import Login from './components/Login.vue'
import Conta from './components/Conta.vue'
import CadastroDuelo from './components/CadastroDuelo.vue'
import Home from './components/Home.vue'

import Vuikit from 'vuikit'
import VuikitIcons from '@vuikit/icons'
import VueRouter from 'vue-router'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import axios from 'axios'
import VueAxios from 'vue-axios'
 
Vue.use(VueAxios, axios)
 
library.add(faSearch)
 
Vue.component('font-awesome-icon', FontAwesomeIcon)

import '@vuikit/theme'

Vue.use(Vuikit)
Vue.use(VuikitIcons)

Vue.use(VueRouter)

const routes = [
  { path: '/duelo', component: Duelo, name: 'Duelo' },
  { path: '/login', component: Login, name: 'Login' },
  { path: '/conta', component: Conta, name: 'Conta'  },
  { path: '/home', component: Home, name: 'Home'  },
  { path: '/cadastro', component: CadastroDuelo, name: 'CadastroDuelo' },
]

const router = new VueRouter({
  routes 
})

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
