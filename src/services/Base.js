const api = 'http://localhost:8000/api/v1'
export default {

 
    listar (ctx, path, success, failed) {
        ctx.$http.get(api + '/' + path  )
            .then(success)
            .catch(failed);
    },
  
    cadastrar (ctx, path, data, success, failed) {
        delete ctx.axios.defaults.headers.post["Content-Type"];
        delete ctx.axios.defaults.headers.post["Accept"];
        delete ctx.axios.defaults.headers.common["Content-Type"];
        delete ctx.axios.defaults.headers.common["Accept"];

        delete ctx.axios.defaults.headers.put["Content-Type"];
        delete ctx.axios.defaults.headers.put["Accept"];

        delete ctx.axios.defaults.headers.patch["Content-Type"];
        delete ctx.axios.defaults.headers.patch["Accept"];
        ctx.axios({
            method: 'post',
            url: api + '/' + path ,
            params: data,
            headers: {}
        }).then(success)
        .catch(failed);
    },


    atualizar (ctx, path, id, data, success, failed) {
        delete ctx.axios.defaults.headers.post["Content-Type"];
        delete ctx.axios.defaults.headers.post["Accept"];
        delete ctx.axios.defaults.headers.common["Content-Type"];
        delete ctx.axios.defaults.headers.common["Accept"];

        delete ctx.axios.defaults.headers.put["Content-Type"];
        delete ctx.axios.defaults.headers.put["Accept"];

        delete ctx.axios.defaults.headers.patch["Content-Type"];
        delete ctx.axios.defaults.headers.patch["Accept"];
        ctx.axios({
            method: 'put',
            url: api + '/' + path + '/' + id,
            params: data,
            config: {headers: {"Authorization": localStorage.token}}
        }).then(success)
        .catch(failed);
    },


    buscarPorId (ctx, path, id, success, failed) {
        delete ctx.axios.defaults.headers.post["Content-Type"];
        delete ctx.axios.defaults.headers.post["Accept"];
        delete ctx.axios.defaults.headers.common["Content-Type"];
        delete ctx.axios.defaults.headers.common["Accept"];

        delete ctx.axios.defaults.headers.put["Content-Type"];
        delete ctx.axios.defaults.headers.put["Accept"];

        delete ctx.axios.defaults.headers.patch["Content-Type"];
        delete ctx.axios.defaults.headers.patch["Accept"];
        ctx.axios({
            method: 'get',
            url: api + '/' + path + '/' + id,
            headers: {"Authorization": localStorage.token}
        }).then(success)
        .catch(failed);
    },


    deletar (ctx, path, id, success, failed) {
        ctx.$http.delete(api + '/' + path + '/' + id)
            .then(success)
            .catch(failed);
    }

}