const api = 'http://localhost:8000/api/v1'
export default {

    // GET Buscar Duelos por Usuário
    buscarDuelosPorUsuario (ctx, id, success, failed) {
        ctx.$http.get(api + '/usuarios/' + id + '/duelos')
            .then(success)
            .catch(failed);
    },
    //GET Buscar Gladiadores por Usuário
    buscarGladiadoresPorUsuario (ctx, id, success, failed) {
        ctx.$http.get(api + '/usuarios/' + id + '/gladiadores')
            .then(success)
            .catch(failed);
    },
}




